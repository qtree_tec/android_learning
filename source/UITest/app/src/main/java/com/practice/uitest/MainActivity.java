package com.practice.uitest;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1,button2,button3,button4;
    private EditText editText1;
    private ImageView imageView1;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button1);
        button1.setOnClickListener(this);

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(this);

        button3 = findViewById(R.id.button3);
        button3.setOnClickListener(this);

        button4 = findViewById(R.id.button4);
        button4.setOnClickListener(this);

        editText1 = findViewById(R.id.editText1);
        imageView1 = findViewById(R.id.imagView1);
        progressBar = findViewById(R.id.progressBar1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1:
                String str = editText1.getText().toString();
                Toast.makeText(this,str, Toast.LENGTH_SHORT).show();
                break;
            case R.id.button2:
                imageView1.setImageResource(R.drawable.img_2);
                break;
            case R.id.button3:
                if(progressBar.getVisibility() == View.GONE){
                    progressBar.setVisibility(View.VISIBLE);
                }else{
                    progressBar.setVisibility(View.GONE);
                }
                break;
            case R.id.button4:
                Intent i = new Intent(MainActivity.this, DialogActivity.class);
                startActivity(i);
                break;
            default:
                break;
        }
    }
}

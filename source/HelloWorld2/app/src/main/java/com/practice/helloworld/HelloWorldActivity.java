package com.practice.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HelloWorldActivity extends AppCompatActivity {
    private static final String TAG = "HelloWorldActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hello_world_layout);
       // TextView textView = findViewById(R.id.myTxtView);
//        Intent intent = getIntent();
//        String data = intent.getStringExtra("myData");
//       // int data1 = intent.getIntExtra("myData1", 0);
//        textView.setText(data);

        Button retBtn = findViewById(R.id.returnBtn);
        retBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = getIntent();
                intent1.putExtra("helloData","我是HelloActivity");
                setResult(RESULT_OK, intent1);
                finish();
            }
        });
    }
}

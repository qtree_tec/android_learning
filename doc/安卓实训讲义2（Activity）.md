## Activity介绍 ##

> **Activity基本用法**

**1.创建Activity**


![](http://39.107.94.138/imgs/activity/activity1.png)

**2.创建布局main_layout.xml**

    
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android
	    android:orientation="vertical" android:layout_width="match_parent"
	    android:layout_height="match_parent">`
    <Button
	    android:layout_width="match_parent"
	    android:layout_height="wrap_content"
	    android:text="按钮1"/>
    </LinearLayout>

**3.在Activity中引用布局**

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.main_layout);
    }
    
> **Activity中使用Toast**
    
**Toast是Android 提供的一种非常好的提醒方式，在程序中可以使用它将一些短小的信息通知到用户，这些信息过一段时间后自动消失，并且不会占用任何屏幕空间。**

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.main_layout);
	    Button button = findViewById(R.id.button1);
	    button.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		    	Toast.makeText(MainActivity.this, "你按了按钮1", Toast.LENGTH_SHORT).show();
		    }
    	});
    }
    
**Toast通过makeText()方法，创建出一个Toast对象，然后调用show()方法显示，三个参数含义如下：**

- **Context:Toast要求的上下文或者说场景，一般来说传入当前类即可。**

- **text:要显示的文本内容**

- Toast显示的时长，有两个内置常量可选择 `Toast.LENGTH_SHOR`T 和 `Toast.LENGTH_LONG`

> **销毁Activity**

**调用finish()方法**

    button.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
	       finish();
	    }
    });

> **使用显式Intent**

Intent 是Android 程序中各组件之间进行交互的重要方式，它不仅可以指明当前组件想要执行的动作，还可以不同组件之间传递数据。我们这里只介绍显式Intent。

代码参考：

    button.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
		    Intent intent = new Intent(MainActivity.this, FirstActivity.class);
		    startActivity(intent);
	    }
    });

Intent 有多个构造函数,其中一个是Intent(Context packageContext, Class<?> cls),第一个构造参数一般是当前Activity作为上下文，第二个参数Class则是指定想要启动哪个目标Activity。Activity类中提供了startActivity()方法，把参数Intent传到这个方法中即可启动目标的Activity。

> **向下一个Activity传递参数**

传递数据的数据通过Intent的putExtra方法进行，把数据暂存到Intent对象中，启动另一个Activity后，只需要把数据再从Intent中取出即可。
传递方法如下：

    button.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
		    Intent intent = new Intent(MainActivity.this, FirstActivity.class);
		    String data = "欢迎报考计算机专业";
		    intent.putExtra("str1",data);
		    startActivity(intent);
	    }
    });

取出方法如下：

我们先通过getIntent方法来获取到启动新的Activity的Intent，然后调用getStringExtra来获取传递的数据，如果传递的数据是布尔型的数据，则使用getBooleanExtra方法，以此类推。
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.first_layout);
	    Intent i= getIntent();
	    String str1 = i.getStringExtra("str1");
	    Log.d(TAG, "onCreate: str1==" + str1);
	    TextView text1 = findViewById(R.id.text1);
	    text1.setText(str1);
    }

> **返回数据到上一个activity**

调用方法为：startActivityForResult，具体代码如下：

**1. 启动Activity时，调用startActivityForResult**

    button2.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
		    Intent intent = new Intent(MainActivity.this, FirstActivity.class);
		    startActivityForResult(intent,1);
	    }
    });
    

**2. 目标Activity返回数据的逻辑**

    btn1.setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
		    Intent intent = new Intent();
		    intent.putExtra("str1", "计算机学院，我来了");
		    setResult(RESULT_OK,intent);
		    finish();
	    }
    });

**3. 返回数据处理**

需要重写onActivityResult方法，里面有三个参数：requestCode是启动目标活动时传入的请求码；resultCode是返回时传入的处理结果；第三个参数data是一个携带数据返回的Intent。

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1:
                if (resultCode == RESULT_OK){
                    String str = data.getStringExtra("str1");
                    textView.setText("返回内容："+ str);
                }
                break;
            default:
        }
    }

> **Activity 生命周期**
如下图：

![](http://39.107.94.138/imgs/activity/activity.gif)

**onCreate()**

当我们点击activity的时候，系统会调用activity的oncreate()方法，在这个方法中我们会初始化当前布局setContentLayout（）方法。
 
**onStart()**

onCreate()方法完成后，此时activity进入onStart()方法,当前activity是用户可见状态，但没有焦点，与用户不能交互，一般可在当前方法做一些动画的初始化操作。
 
**onResume()**

onStart()方法完成之后，此时activity进入onResume()方法中，当前activity状态属于运行状态 (Running)，可与用户进行交互。 

**onPause()**

当另外一个activity覆盖当前的acitivty时，此时当前activity会进入到onPouse()方法中，当前activity是可见的，但不能与用户交互状态。
 
**onStop()**

onPouse()方法完成之后，此时activity进入onStop()方法，此时activity对用户是不可见的，在系统内存紧张的情况
下，有可能会被系统进行回收。所以一般在当前方法可做资源回收。 

**onDestory()**

onStop()方法完成之后，此时activity进入到onDestory()方法中，结束当前activity。 

**onRestart()**

onRestart()方法在用户按下home()之后，再次进入到当前activity的时候调用。调用顺序onPouse()->onStop()->onRestart()->onStart()->onResume().

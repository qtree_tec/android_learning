## UI界面 ##

> **常用控件**

**1.TextView**

	<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    android:orientation="vertical"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent">
    	<TextView
	        android:id="@+id/returnStr"
	        android:layout_width="match_parent"
	        android:layout_height="wrap_content"
	        android:gravity="center"
	        android:textSize="24sp"
	        android:textColor="#ff0000"
	        android:text="This is TextView"/>
	</LinearLayout>

**android:gravity 指定文字对齐方式：top、bottom、left、right、center等，可以用“|”来同时指定多个值**

**android:textSize 指定字体大小，Android中字体大小使用sp作为单位**

**android:textColor 指定文字颜色**

**android:textStyle 字体样式：bold 粗体、italic 斜体、nomal 常规**

**2.Button**

    <Button
	    android:id="@+id/button1"
	    android:layout_width="match_parent"
	    android:layout_height="wrap_content"
	    android:text="button1"
		android:textAllCaps="false"/>

**android:textAllCaps 此属性默认为true，会对Button中的所有英文字母自动进行大写转换**

**Button的接口方式进行注册：**

    public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	    private Button button1;
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);
	        button1 = findViewById(R.id.button1);
	        button1.setOnClickListener(this);
	    }
	
	
	    @Override
	    public void onClick(View v) {
	        switch (v.getId()){
	            case R.id.button1:
	                Toast.makeText(this, "点击了按钮1", Toast.LENGTH_SHORT).show();
	                break;
	            default:
	                break;
	        }
	    }
	}


**3.EditText**

布局文件如下：

	 <EditText
        android:id="@+id/edit_text"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:hint="请输入文字"
        android:maxLines="2"
        />

activity中获取内容：

	String inputText = editText.getText().toString();

**android:hint 提示文字。**

**android:maxLines   指定EditText的最大行数。**

**4.ImageView**

 	<ImageView
        android:id="@+id/image_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/img_1"
        />

**android：src 指定图片的位置，通常图片都放在res/drawable目录下**

	imageView1.setImageResource(R.drawable.img_2);

**通过setImageResource()方法更换图片**

**5.ProgressBar 进度条**

	 <ProgressBar
         android:id="@+id/progressBar1"
         android:layout_width="match_parent"
         android:layout_height="wrap_content" />

**控件是否显示，通过android:visibility进行指定，可选值为：visible、invisible、gone 。 visible表示可见的；invisible表示不可见，但是占据位置和大小；gone则表示不可见且不占用屏幕空间，通过代码控制控件是否显示则使用setVisibility()，可以传入View.GONE,View.INVISIBLE和View.GONE这三种值。**

**5.AlertDialog**
	
	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    alertDialog.setTitle("这里是标题");
    alertDialog.setMessage("这里是提示信息");
    alertDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Toast.makeText(DialogActivity.this, "你点击了确定", Toast.LENGTH_SHORT).show();
        }
    });
    alertDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Toast.makeText(DialogActivity.this, "你点击了取消", Toast.LENGTH_SHORT).show();
        }
    });
    alertDialog.show();

**6.ProgressDialog**
	
	ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
	progressDialog.setTitle("This is ProgressDialog");
	progressDialog.setMessage("Loading...");
	progressDialog.setCancelable(true);
	progressDialog.show();


> **基本布局**

布局和控件是可以多层嵌套的，布局的内部不仅可以放置控件，还可以放置布局。

![](http://39.107.94.138/imgs/layout/layout.png)

**1. 线性布局（LinearLayout）**

在这个布局中，所有的控件都是沿一个方向排列。

参数android:orientation 属性指定排列方向，vertical垂直排列，horizontal 水平排列。

android:layout_weight  允许我们通过比例来指定控制的大小。

	<LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <EditText
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:hint="请输入消息"/>
        <Button
            android:text="确定"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            />
    </LinearLayout>

**当水平方向的两个控件一个设置了 android:layout_weight属性，另一个的宽度仍然是wrap_content，如以上代码，EditText就回占用屏幕的剩余空间，这种方式编写的界面，适配非常好，而且看起来更加舒服。**

**2. 相对布局（RelativeLayout）**
	
**通过相对定位的方式，让控件出现在布局的任何位置。**

相对父布局进行定位，布局代码如下：

	<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    xmlns:app="http://schemas.android.com/apk/res-auto"
	    xmlns:tools="http://schemas.android.com/tools"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent"
	    tools:context=".RelativeActivity">
	    <Button
	        android:id="@+id/button1"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_alignParentLeft="true"
	        android:layout_alignParentTop="true"
	        style="@style/button"
	        android:text="button1"/>
	    <Button
	        android:id="@+id/button2"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_alignParentRight="true"
	        android:layout_alignParentTop="true"
	        style="@style/button"
	        android:text="button2"/>
	    <Button
	        android:id="@+id/button3"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_centerInParent="true"
	        style="@style/button"
	        android:text="button3"/>
	    <Button
	        android:id="@+id/button4"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_alignParentLeft="true"
	        android:layout_alignParentBottom="true"
	        style="@style/button"
	        android:text="button4"/>
	    <Button
	        android:id="@+id/button5"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_alignParentRight="true"
	        android:layout_alignParentBottom="true"
	        style="@style/button"
	        android:text="button5"/>
	</RelativeLayout>


相对控件进行定位，布局代码如下：

	<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    xmlns:app="http://schemas.android.com/apk/res-auto"
	    xmlns:tools="http://schemas.android.com/tools"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent"
	    tools:context=".RelativeActivity">
	    <Button
	        android:id="@+id/button1"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_above="@id/button3"
	        android:layout_toLeftOf="@id/button3"
	        style="@style/button"
	        android:text="button1"/>
	    <Button
	        android:id="@+id/button2"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_above="@id/button3"
	        android:layout_toRightOf="@id/button3"
	        style="@style/button"
	        android:text="button2"/>
	    <Button
	        android:id="@+id/button3"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_centerInParent="true"
	        style="@style/button"
	        android:text="button3"/>
	    <Button
	        android:id="@+id/button4"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_below="@id/button3"
	        android:layout_toLeftOf="@id/button3"
	        style="@style/button"
	        android:text="button4"/>
	    <Button
	        android:id="@+id/button5"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_below="@id/button3"
	        android:layout_toRightOf ="@id/button3"
	        style="@style/button"
	        android:text="button5"/>
	</RelativeLayout>

> **引入布局**

**当很多布局属于重复的内容，我们可以抽出来，单独做布局，然后在布局文件中引用即可。引用方式很简单，如下代码：**

  	<include layout="@layout/title" />

> **最常用和最难用的控件——ListView**

**ListView，列表视图，直接继承了AbsListView，是一个以垂直方式在项目中显示View视图的列表。ListView的数据项，来自一个继承了ListAdapter接口的适配器。当我们的程序中有大量的数据需要现实的时候，就可以借助ListView来实现。**

**1. 简单实现**

**修改activity_main.xml 中的代码，如下所示**
	
	<?xml version="1.0" encoding="utf-8"?>
	<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    android:orientation="vertical"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent">
	    <ListView
	        android:layout_width="match_parent"
	        android:layout_height="match_parent"
	        android:id="@+id/list_view" />
	</LinearLayout>

**修改MainActivity中的代码，如下所示**

	public class MainActivity extends AppCompatActivity {

	    private String[] data = { "Apple", "Banana", "Orange", "Watermelon",
	            "Pear", "Grape", "Pineapple", "Strawberry", "Cherry", "Mango" };
	
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
	            MainActivity.this, android.R.layout.simple_list_item_1, data);
	        ListView listView = (ListView) findViewById(R.id.list_view);
	        listView.setAdapter(adapter);
	    }
	}

(1) 在这里我运用了系统包含的一个TextView的布局文件：android.R.layout.simple_expandable_list_item_1,调用这个比较方便，

(2) ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, data); 的意思是：创建一个数组适配器的代码，里面有三个参数：

第一个参数是上下文，就是当前的Activity；

第二个参数是android sdk中自己内置的一个布局，它里面只有一个TextView，这个参数是表明我们数组中每一条数据的布局是这个view，就是将每一条数据都显示在这个 view上面；

第三个参数就是我们要显示的数据。listView会根据这三个参数，遍历data里面的每一条数据，读出一条，显示到第二 个参数对应的布局中，这样就形成了我们看到的listView.

(3) ArrayAdapter是BaseAdapter的子类

**2.定制ListView界面**

**主要参考步骤及代码**

**1) 定义一个实体类 Fruit，作为 ListView 适配器的适配类型。**

**2) 为 ListView 的子项指定一个我们自定义的布局 fruit_item.xml。**

**3) 创建一个自定义的适配器 FruitAdapter，这个适配器继承自 ArrayAdapter。重写构造方法和 getView 方法。**

**4)在MainActivity中编写，初始化水果数据**

**5)在MainActivity中编写，重新设置Adapter**

下面是具体步骤：

（1）定义一个实体类Fruit

	public class Fruit {

	    private String name;
	    private int imageId;
	
	    public String getName() {
	        return name;
	    }
	
	    public void setName(String name) {
	        this.name = name;
	    }
	
	    public int getImageId() {
	        return imageId;
	    }
	
	    public void setImageId(int imageId) {
	        this.imageId = imageId;
	    }
	}

（2）为 ListView 的子项指定一个我们自定义的布局 fruit_item.xml

	<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
	    android:orientation="horizontal"
	    android:layout_width="match_parent"
	    android:layout_height="match_parent">
	
	    <ImageView
	        android:id="@+id/fruit_image"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content" />
	
	    <TextView
	        android:id="@+id/fruit_name"
	        android:layout_width="wrap_content"
	        android:layout_height="wrap_content"
	        android:layout_gravity="center"
	        android:layout_marginLeft="10dip" />
	
	</LinearLayout>

（3）创建一个自定义的适配器 FruitAdapter，这个适配器继承自 ArrayAdapter。重写构造方法和 getView 方法。

	public class FruitAdapter extends ArrayAdapter {
	    private final int resourceId;
	
	    public FruitAdapter(Context context, int textViewResourceId, List<Fruit> objects) {
	        super(context, textViewResourceId, objects);
	        resourceId = textViewResourceId;
	    }
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        Fruit fruit = (Fruit) getItem(position); // 获取当前项的Fruit实例
	        View view = LayoutInflater.from(getContext()).inflate(resourceId, null);//实例化一个对象
	        ImageView fruitImage = (ImageView) view.findViewById(R.id.fruit_image);//获取该布局内的图片视图
	        TextView fruitName = (TextView) view.findViewById(R.id.fruit_name);//获取该布局内的文本视图
	        fruitImage.setImageResource(fruit.getImageId());//为图片视图设置图片资源
	        fruitName.setText(fruit.getName());//为文本视图设置文本内容
	        return view;
	    }
	}

View view = LayoutInflater.from(getContext()).inflate(resourceId, null);使用Inflater对象来将布局文件解析成一个View.

（4）在MainActivity中编写，初始化水果数据

	private void initFruits() {
        Fruit apple = new Fruit("Apple", R.drawable.apple_pic);
        fruitList.add(apple);
        Fruit banana = new Fruit("Banana", R.drawable.banana_pic);
        fruitList.add(banana);
        Fruit orange = new Fruit("Orange", R.drawable.orange_pic);
        fruitList.add(orange);
        Fruit watermelon = new Fruit("Watermelon", R.drawable.watermelon_pic);
        fruitList.add(watermelon);
        Fruit pear = new Fruit("Pear", R.drawable.pear_pic);
        fruitList.add(pear);
        Fruit grape = new Fruit("Grape", R.drawable.grape_pic);
        fruitList.add(grape);
        Fruit pineapple = new Fruit("Pineapple", R.drawable.pineapple_pic);
        fruitList.add(pineapple);
        Fruit strawberry = new Fruit("Strawberry", R.drawable.strawberry_pic);
        fruitList.add(strawberry);
        Fruit cherry = new Fruit("Cherry", R.drawable.cherry_pic);
        fruitList.add(cherry);
        Fruit mango = new Fruit("Mango", R.drawable.mango_pic);
        fruitList.add(mango);
    }

(5)在MainActivity中编写，重新设置Adapter。

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFruits();
        FruitAdapter adapter = new FruitAdapter(MainActivity.this, R.layout.fruit_item, fruitList);
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
    }

**3.ListView点击事件**

使用 setOnItemClickListener 为ListView 注册一个监听器，当用户点击任何子项的时候，就会回调ItemClick方法，在这个方法中，通过position参数来判断用户点击的哪一个子项，然后获取到相应的水果，通过Toast将水果的名字显示出来。

	listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fruit fruit = fruitList.get(position);
            Toast.makeText(MainActivity.this,fruit.getName(), Toast.LENGTH_SHORT).show();
        }
    });

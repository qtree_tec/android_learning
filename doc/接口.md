##接口文档##

> **1.登录**

**URL: http://panda.treeqe.com/app/userLogin.do**

**method：get/post**

**请求参数： code：账号   password: 密码  （正确的：test/123456）**

**返回结果：**

正确结果如下：
	
	{
	    "result": {
	        "NAME": "测试",  //账号名
	        "YZUSER_ID": "4e58fd4a87964412984cb0e7b2a87479", //用户ID
	        "GROUT_REMOTE_FLAG": 0,
	        "REMARK": "",  //备注
	        "REMOTE_PASSWORD": "",
	        "ADDTIME": "2018-05-08 07:45:32",  //添加时间
	        "REMOTE_URL": "http://222.73.236.136:8207/taishun/tensionGroution.do",
	        "STATUS": 0,  
	        "GROUT_REMOTE_STRATEGE": 0,
	        "REMOTE_STRATEGE": 1,
	        "MOBILE": "13931129291",  //联系人手机号
	        "LINKMAN": "测试",  //联系人
	        "REMOTE_FLAG": 1,
	        "REMOTE_CODE": "",
	        "CODE": "test"  //账号
	    },
	    "success": true  //调用成功
	}

错误结果如下：

	{
	    "msg": "密码不能为空",
	    "success": false  //调用成功
	}



> 2.每日一图

**URL: http://guolin.tech/api/bing_pic**

**method：get/post**


**返回结果：**


    http://cn.bing.com/az/hprichbg/rb/TDPflamingos_ZH-CN9958639267_1920x1080.jpg



> 3.获取省信息

**URL: http://guolin.tech/api/china**

**method：get/post**

**返回结果：**

正确结果如下：
	
	[
	    {
	        "id": 1,
	        "name": "北京"
	    },
	    {
	        "id": 2,
	        "name": "上海"
	    },
	    {
	        "id": 3,
	        "name": "天津"
	    },
	    {
	        "id": 4,
	        "name": "重庆"
	    },
	    {
	        "id": 5,
	        "name": "香港"
	    },
	    {
	        "id": 6,
	        "name": "澳门"
	    },
	    {
	        "id": 7,
	        "name": "台湾"
	    },
	    {
	        "id": 8,
	        "name": "黑龙江"
	    },
	    {
	        "id": 9,
	        "name": "吉林"
	    },
	    {
	        "id": 10,
	        "name": "辽宁"
	    },
	    {
	        "id": 11,
	        "name": "内蒙古"
	    },
	    {
	        "id": 12,
	        "name": "河北"
	    }
	]

> 3.获取市信息


**URL: http://guolin.tech/api/china/12**

**method：get/post**

**返回结果：**

正确结果如下：
	
	[
	    {
	        "id": 57,
	        "name": "石家庄"
	    },
	    {
	        "id": 58,
	        "name": "保定"
	    },
	    {
	        "id": 59,
	        "name": "张家口"
	    },
	    {
	        "id": 60,
	        "name": "唐山"
	    },
	    {
	        "id": 61,
	        "name": "廊坊"
	    },
	    {
	        "id": 62,
	        "name": "沧州"
	    },
	    {
	        "id": 63,
	        "name": "衡水"
	    },
	    {
	        "id": 64,
	        "name": "邢台"
	    },
	    {
	        "id": 65,
	        "name": "邯郸"
	    },
	    {
	        "id": 66,
	        "name": "秦皇岛"
	    }
	]


